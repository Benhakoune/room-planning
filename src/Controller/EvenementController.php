<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Evenement;
use App\Form\EvenementType;
use DateInterval;
use DateTime;
use DateTimeImmutable;


class EvenementController extends AbstractController
{
    /**
     * @Route("/evenement/{id}", name="show_evenement", requirements={"id"="\d+"})
     */
    public function show($id): Response
    {
        return $this->render('evenement/show.html.twig',['evenement_id'=>$id]);
    }

    /**
      * @Route("/evenement/create", name="create_evenement")
      */

        public function createEvenement() : Response{
        $evenement = new Evenement();
        $evenement->setName('Evenement1');
        $evenement->setDuration(DateInterval::createFromDateString('1 day'));
        $evenement->setNbPeople(10);
        $evenement->setDate(new DateTimeImmutable());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($evenement);
        $entityManager->flush();



        return new Response("L'evenement {$evenement->getName()} a bien été enregistré");


    }




    public function index()
    {
        return $this->render('evenement/add.html.twig');
    }

    /**
      * @Route("/evenement/add", name="add_evenement")
      */

    public function add(){

    $evenement = new Evenement();
    $form = $this->createForm(EvenementType::class, $evenement);

    return $this->render('evenement/add.html.twig');


}



}
