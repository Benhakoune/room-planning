<?php
// src/Controller/CoreController.php

namespace App\Controller;

use App\Repository\EvenementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoreController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage(EvenementRepository $evenementRepository): Response
    {
        $evenements = $evenementRepository->findAll();
        return $this->render('core/index.html.twig',['evenements'=>$evenements]);
    }

    public function about(){
        return new Response("<h1>Projet roomplanning par Benjamin et Hicham<h1>");
    }
}
